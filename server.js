const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
app.listen(port);
const bodyParser = require('body-parser');
app.use(bodyParser.json());
console.log("Api escuchando en el BEEEEEP "+port);

app.get('/apitechu/v1/hello',

  function(req,res){
    //req request res response
    console.log("GET /apitechu/v1/hello'");
    res.send({"msg" : "Hola desde apitechu"});
  }
);
app.get("/apitechu/v1/users",
 function(req, res) {
   console.log("GET /apitechu/v1/users");
   console.log(req.query);

   var result = {};
   var users = require('./usuarios.json');

   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length;
   }

   result.users = req.query.$top ?
      users.slice(0, req.query.$top) : users;

   res.send(result);
 }
);

  app.post('/apitechu/v1/users',

    function(req,res){
      //req request res response
      console.log("POST /apitechu/v1/users'");
      //res.sendFile('usuarios.json',{root:__dirname});
      //console.log(req.headers);
      console.log("first_name es " + req.body.first_name);
      console.log("last_name es " + req.body.last_name);
      console.log("email es " + req.body.email);
      var newUser = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email
      }
      var users = require('./usuarios.json');
      users.push(newUser);
      writeUserDataToFile(users);
      console.log("Usuario añadido con éxito");
      res.send({"msg":"Usuario Añadido Con éxito"});
    }
  );
  app.post('/apitechu/v1/login',

    function(req,res){
      console.log("POST /apitechu/v1/login'");
      //res.sendFile('usuarios.json',{root:__dirname});
      //console.log(req.headers);
      console.log("email es " + req.body.email);
      var users = require('./loginUsuarios.json');

      for (user of users){
        //var usuario = users.valueOf(i);
        if (user!=null && user.email == req.body.email){
          //borro
          console.log("el usuario "+ user.first_name +" "+user.last_name + " se ha logado");
          user.logged = true;//esto no vale porque es un poco ñapa suponiendo que viene siempre con el mismo orden?
          break;
        }
      }
      if(user.logged !=null){
        //

        if (user.password == req.body.password){
          writeUserDataToFile(users);
          console.log("El usuario esta logado");
          var respuesta = {
            "msg" : "Login Correcto",
            "id"  : user.id
          };
          res.send(respuesta);
        }else{
          console.log("El usuario no esta logado");
          res.send("msg : login incorrecto");
        }
      }else{
        //el usuario no esta logado
        console.log("El usuario no existe");
        res.send("msg : login incorrecto");
      }
    }
  );
  app.post('/apitechu/v1/logout',

    function(req,res){
      console.log("POST /apitechu/v1/logout'");
      //res.sendFile('usuarios.json',{root:__dirname});
      //console.log(req.headers);
      console.log("id para deslogar es " + req.body.id);
      var users = require('./loginUsuarios.json');

      for (user of users){
        //var usuario = users.valueOf(i);
        if (user!=null && user.id == req.body.id){
          //borro
          console.log("el usuario "+ user.first_name +" "+user.last_name + " se ha deslogado");
          delete user.logged;//esto no vale porque es un poco ñapa suponiendo que viene siempre con el mismo orden?
          break;
        }
      }
      if(user.id == req.body.id){
        //

          writeUserDataToFile(users);
          console.log("El usuario esta deslogado");
          var respuesta = {
            "msg" : "Logout Correcto",
            "id"  : user.id
          };
          res.send(respuesta);
      }else{
          console.log("El usuario no esta logado");
          res.send("msg : logout incorrecto");
      }

    }
  );
  app.delete('/apitechu/v1/users/:id',

    function(req,res){
      //req request res response
      console.log("DELETE /apitechu/v1/users:id'");
      console.log("id es para borrar:" + req.params.id);
      var users = require('./usuarios.json');
      //var users = usersJson.Parse();
      //users.splice(req.params.id -1,1);
      var idBorrar = req.params.id;
      //var i=0;
      for (user of users){
        //var usuario = users.valueOf(i);
        if (user!=null && user.id == idBorrar){
          //borro
          console.log("borro el id "+ user.id + "el valor de la variable es " +idBorrar );
          delete users[user.id-1];//esto no vale porque es un poco ñapa suponiendo que viene siempre con el mismo orden?
          break;
        }
          //user+=1;
        //}
      }
      console.log("Usuario borrado");
      //writeUserDataToFile(users);
      res.send({"msg":"Usuario borrado Con éxito"});

/*
*/

    }
  );
  app.post('/apitechu/v1/monstruo/:p1/:p2',

    function(req,res){
      //req request res response
      console.log("POST /apitechu/v1/monstruo/:p1/p2");
      //res.sendFile('usuarios.json',{root:__dirname});
      console.log("Parametros");
      console.log(req.params);

      console.log("QueryString");
      console.log(req.query);

      console.log("Headers");
      console.log(req.headers);

      console.log("Body");
      console.log(req.body);
    }
  );

  function writeUserDataToFile(data){
    const fs = require('fs');
    var jsonUserData = JSON.stringify(data);
    fs.writeFile("./usuarios.json",jsonUserData,"utf8",

      function(err){
        if(err){
          console.log(err)
        }else{
          console.log("Datos escritos en fichero")
        }
      });


  }


  app.get('/apitechu/v1/users/?top&&?count',

    function(req,res){

      console.log("QueryString");
      console.log(req.query);
      console.log("req.params.top es = " + req.params.top);
      var primerosN = req.params.top;
      var users = require('./usuarios.json');
      var auxUsers = new Array();
      if (req.params.top !=null && !isnan(req.params.top)){
        console.log("top es un numero");
          //recorro del json los top elementos -1 por empezar desde 0
          /*var maxElementos;
          if primerosN>users.length{
            maxElementos = users.length
          }else{
            maxElementos = primerosN
          }
          var i;

          for (i = 0; i < maxElementos; i++) {
              auxUsers.push(users[i]);
          }

          */
      }else{
        //relleno respuesta error
        console.log("no es un numero el parametro querystring top");
      }


    }
  );
